PREFIX=/usr/local

all: install

install:
	@mkdir -p ${PREFIX}/bin
	@install -D -m 0755 processlog ${PREFIX}/bin/
	@install -D -m 0644 processlog-logrotate.conf /etc/logrotate.d/processlog.conf
	@install -D -m 0644 processlog.service /usr/lib/systemd/system

uninstall:
	@rm ${PREFIX}/bin/processlog
	@rm /etc/logrotate.d/processlog.conf
	@rm /usr/lib/systemd/system/processlog.service
